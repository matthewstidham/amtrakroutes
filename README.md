# cities
For the DBSCAN project:
Step 1: 'all cities csv generator.ipynb' creates 'all_cities.csv'
Step 2: 'USA csv generator.ipynb' creates 'usa.csv'
Step 3: 'networkx usa.pynb' uses networkx to generate the most efficient lines to connect every American.