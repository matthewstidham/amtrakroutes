#!/usr/bin/env python3
import pandas as pd
import itertools
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt

class Filters:
    def city(self,value, df):
        return pd.concat([df[df['city'] == str(value)], df[df['city_x'] == str(value)]]).sort_values('power',
                                                                                                     ascending=False)
    
    
    def province(self, value, df):
        return pd.concat([df[df['province'] == str(value)],
                          df[df['province_x'] == str(value)]]).sort_values('power', ascending=False)
    
    
    def country(self, value, df):
        return pd.concat([df[df['country'] == str(value)], df[df['country_x'] == str(value)]]).sort_values('power',
                                                                                                           ascending=False)
    
    
    def population(self, value, df):
        return pd.concat([df[df['pop_x'] > value], df[df['pop'] > value]]).sort_values('power', ascending=False)
    
    
    def populationrange(self, value1, value2, df):
        return pd.concat([df[df['pop_x'].between(self, value1, value2, inclusive=True)],
                          df[df['pop'].between(self, value1, value2, inclusive=True)]]).sort_values('power',
                                                                                              ascending=False).drop_duplicates()
    
    
    def provincetoprovince(self, value1, value2, df):
        return pd.concat([df[df['province'] == value1][df['province_x'] == value2],
                          df[df['province_x'] == value1][df['province'] == value2]]).sort_values('power',
                                                                                                 ascending=False)
    
    
    def citytocity(self, value1, value2, df):
        return pd.concat([df[df['city'] == value1][df['city_x'] == value2],
                          df[df['city_x'] == value1][df['city'] == value2]]).sort_values('power', ascending=False)
    
    
    def province2(self, value, df):
        return df[df['province'] == str(value)][df['province_x'] == str(value)]
    
    
    def province3(self, value, df):
        return pd.concat([df[df['province'] == str(value)], df[df['province_x'] == str(value)]]).sort_values('power',
                                                                                                             ascending=False)
    
    
    def country2(self, value, df):
        return df[df['country'] == str(value)][df['country_x'] == str(value)]
    
    
    def closestlarger(self, df):
        return df.sort_values(['city_x', 'distance']).drop_duplicates('city_x').sort_values('distance', ascending=False)
    
    
    def plotter(self, df):
        figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
        for n in range(0, df.shape[0]):
            plt.plot([df.iloc[n]['lng'], df.iloc[n]['lng_x']], [df.iloc[n]['lat'], df.iloc[n]['lat_x']], marker='o')
        plt.show()
